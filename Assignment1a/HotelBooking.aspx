﻿    <%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HotelBooking.aspx.cs" Inherits="Assignment1a.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml", lang="en">
<head runat="server">
    <meta charset="utf-8" />
    <title>Hotel Booking Page</title>
    <link href="https://fonts.googleapis.com/css?family=Poiret+One" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet" />
    <link href="Layout.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <main>
        <header>
            <h1>Book your room(s)</h1>
        </header>
        <form id="form1" runat="server" method="post" action="#">
            <section id="sectionCheckInOut">
                <div>
                    <asp:Label AssociatedControlID="checkIn" Text="Check-in" runat="server"></asp:Label>
                    <asp:TextBox id="checkIn" runat="server" TextMode="Date"></asp:TextBox>
                    <asp:RequiredFieldValidator ControlToValidate="checkIn" ID="validatorCheckIn" runat="server" 
                        ErrorMessage="Provide check-in date" CssClass="errorMsg"></asp:RequiredFieldValidator>
                </div>
                <div>
                    <asp:Label AssociatedControlID="checkOut" Text="Check-out" runat="server"></asp:Label>
                    <asp:TextBox id="checkOut" runat="server" TextMode="Date"></asp:TextBox>
                    <asp:RequiredFieldValidator ControlToValidate="checkOut" ID="validatorCheckOut" runat="server" 
                        ErrorMessage="Provide check-out date" CssClass="errorMsg"></asp:RequiredFieldValidator>
                </div>
            </section>
            <section>
                <div>
                <asp:Label AssociatedControlID="clientName" Text="Full Name" runat="server"></asp:Label>
                <asp:TextBox ID="clientName" runat="server" placeholder="eg: John Doe"></asp:TextBox>
                <asp:RequiredFieldValidator ControlToValidate="clientName" ID="validatorClientName" runat="server" 
                    ErrorMessage="Provide your full name" CssClass="errorMsg"></asp:RequiredFieldValidator>
            </div>
            <div>
                <asp:Label AssociatedControlID="clientAddress" Text="Address" runat="server"></asp:Label>
                <asp:TextBox ID="clientAddress" runat="server" placeholder="eg: Toronto, ON"></asp:TextBox>
                <asp:RequiredFieldValidator ControlToValidate="clientAddress" ID="validatorClientAddress" runat="server" 
                    ErrorMessage="Provide your address" CssClass="errorMsg"></asp:RequiredFieldValidator>
            </div>
            <div>
                <asp:Label AssociatedControlID="clientPhone" Text="Phone" runat="server"></asp:Label>
                <asp:TextBox ID="clientPhone" runat="server" placeholder="eg: 123-456-7890"></asp:TextBox>
                <asp:RequiredFieldValidator ControlToValidate="clientPhone" ID="validatorClientPhone" runat="server" 
                    ErrorMessage="Provide your contact phone number" CssClass="errorMsg"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ControlToValidate="clientPhone" runat="server" 
                    ValidationExpression="^\d{3}-\d{3}-\d{4}$" ID="expValidatorClientPhone" 
                    ErrorMessage="Invalid phone number" CssClass="errorMsg"></asp:RegularExpressionValidator>
            </div>
            <div>
                <asp:Label AssociatedControlID="clientEmail" Text="Email" runat="server"></asp:Label>
                <asp:TextBox ID="clientEmail" runat="server" TextMode="Email" placeholder="eg: example@example.com" Width="200px"></asp:TextBox>
                <asp:RequiredFieldValidator ControlToValidate="clientEmail" ID="validatorClientEmail" runat="server" 
                    ErrorMessage="Provide your contact email address" CssClass="errorMsg"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ControlToValidate="clientEmail" runat="server" 
                    ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                    ID="expValidatorClientEmail" ErrorMessage="Invalid email address" CssClass="errorMsg"></asp:RegularExpressionValidator>
                <asp:CompareValidator ControlToValidate="clientEmail" runat="server" Type="String" 
                    Operator="NotEqual" ValueToCompare="bibekmanshrestha@gmail.com" 
                    ErrorMessage="You are the owner. You don't need to book a room!" CssClass="errorMsg"></asp:CompareValidator>
            </div>
            </section>
            <section>
                <div>
                <asp:Label AssociatedControlID="NumRooms" Text="Rooms" runat="server"></asp:Label>
                <asp:DropDownList ID="NumRooms" runat="server">
                    <asp:ListItem Enabled="true" Text="1" Value="1"></asp:ListItem>
                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div>
                <asp:Label AssociatedControlID="NumAdults" Text="Adults" runat="server"></asp:Label>
                <asp:DropDownList ID="NumAdults" runat="server">
                    <asp:ListItem Enabled="true" Text="1" Value="1"></asp:ListItem>
                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div>
                <asp:Label AssociatedControlID="NumChild" Text="Children" runat="server"></asp:Label>
                <asp:DropDownList ID="NumChild" runat="server">
                    <asp:ListItem Enabled="true" Text="0" Value="0"></asp:ListItem>
                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div>
                <asp:Label AssociatedControlID="TypeRoom" Text="Type of room" runat="server"></asp:Label>
                <asp:DropDownList ID="TypeRoom" runat="server">
                    <asp:ListItem Enabled="true" Text="Deluxe Room" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Executive Room" Value="2"></asp:ListItem>
                    <asp:ListItem Text="Premier Room" Value="3"></asp:ListItem>
                    <asp:ListItem Text="One Bedroom Suite" Value="4"></asp:ListItem>
                    <asp:ListItem Text="One Bedroom Deluxe Suite" Value="5"></asp:ListItem>
                    <asp:ListItem Text="Speciality Suite" Value="6"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div>
                <asp:Label AssociatedControlID="TypeBed" Text="Type of bed" runat="server"></asp:Label>
                <asp:RadioButtonList ID="TypeBed" runat="server" RepeatColumns="2" RepeatDirection="Horizontal">
                    <asp:ListItem Selected="True" Value="King" Text="King Bed"/>
                    <asp:ListItem Value="Twin" Text="Twin Bed"/>
                </asp:RadioButtonList>
            </div>
            </section>
            <section>
                <div>
                <asp:Label AssociatedControlID="Meal" Text="Would you like to include any meal?" runat="server"></asp:Label>
                <asp:CheckBoxList ID="Meal" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
                    <asp:ListItem Text="Breakfast" Value="Breakfast" />
                    <asp:ListItem Text="Lunch" Value="Lunch" />
                    <asp:ListItem Text="Snacks" Value="Snacks" />
                </asp:CheckBoxList>
            </div>

            <div>
                <asp:Label AssociatedControlID="PetQuery" Text="Do you have a pet?" runat="server"></asp:Label>
                <asp:RadioButtonList ID="PetQuery" runat="server" RepeatColumns="2" RepeatDirection="Horizontal">
                    <asp:ListItem Selected="True" Text="No" Value="No"/>
                    <asp:ListItem Text="Yes" Value="No"/>
                </asp:RadioButtonList>
            </div>
            <div>
                <asp:Label AssociatedControlID="PetKind" Text="If yes, What kind?" runat="server"></asp:Label>
                <asp:CheckBoxList ID="PetKind" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
                    <asp:ListItem Text="Dog" Value="Dog" />
                    <asp:ListItem Text="Cat" Value="Cat" />
                    <asp:ListItem Text="Other" Value="Other" />
                </asp:CheckBoxList>
            </div>
            </section>
            <section>
                <asp:Button runat="server" ID="submitButton" Text="Submit"/>
                <asp:ValidationSummary runat="server" DisplayMode="List" EnableClientScript="true" ShowSummary="true" CssClass="errorMsg errorSummary"/>
            </section>
        </form>
    </main>
</body>
</html>
